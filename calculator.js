function Calculator(){}

Calculator.prototype.sumNumbers = function (arr)
{
	var sum = 0;

	if (arr.length == 0)
	{
		throw new UserException('Empty Array');
	}

	for (var i = 0; i < arr.length; i++)
	{
		sum += arr[i];
	}

	return sum;
};

Calculator.prototype.greaterThan = function(num1, num2) {
	if (num1 > num2)
	{
		return true;
	}
	else if (num1 == num2)
	{
		throw new UserException('Numbers Equivalent');
	}
	else
	{
		return false;
	}
};

Calculator.prototype.lessThan = function(num1, num2) {
	if (num1 < num2)
	{
		return true;
	}
	else if (num1 == num2)
	{
		throw new UserException('Numbers Equivalent');
	}
	else 
	{
		return false;
	}
};


Calculator.prototype.add = function (num1, num2)
{
	return num1+num2;
};

Calculator.prototype.subtract = function (num1, num2)
{
	return num1-num2;
}

Calculator.prototype.multiply = function (num1, num2)
{
	return num1*num2;
}

Calculator.prototype.divide = function (num1, num2)
{
	return num1/num2;
}

Calculator.prototype.stringLength = function (string1)
{
	return string1.length;
}


module.exports = Calculator;
