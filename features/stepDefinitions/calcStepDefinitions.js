let {Given, When, Then} = require('cucumber');
let {After, Before} = require('cucumber');
let assert = require('assert');
let calculator = require('../../Calculator');

var int1;
var int2;
var actual;
var calc;

Before(function()   {
    int1 = 0;
    actual = 0;
    calc = new calculator();
});

Given('The first number {int}', function(num1)  {
    int1 = num1;
});

Given('The second number {int}', function(num2) {
    int2 = num2;
});

When('I add two numbers together', function()  {
    actual = calc.add(int1, int2);
});

When('I subtract one number from another', function()  {
    actual = calc.subtract(int1, int2);
});

When('I multiply two numbers together', function()  {
    actual = calc.multiply(int1, int2);
});

When('I divide one number from another', function()  {
    actual = calc.divide(int1, int2);
});

Then('I get the result {int}', function(expected)   {
    assert.equal(expected, actual);
})