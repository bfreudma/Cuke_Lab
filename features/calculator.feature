Feature: Calculator
This feature tests the functionality of the calculator app

Background:
    Given The first number 100

Scenario Outline: Addition
    Given The second number <num1>
    When I add two numbers together
    Then I get the result <result>

    Examples:
    | num1 | result |
    | 2    | 102    |
    | 10   | 110    |
    | 25   | 125    |

Scenario Outline: Subraction
    Given The second number <num1>
    When I subtract one number from another
    Then I get the result <result>

    Examples:
    | num1 | result |
    | 2    | 98     |
    | 10   | 90     |
    | 25   | 75     |

Scenario Outline: Multiplication
    Given The second number <num1>
    When I multiply two numbers together
    Then I get the result <result>

    Examples:
    | num1 | result |
    | 2    | 200    |
    | 10   | 1000   |
    | 25   | 2500   |

Scenario Outline: Division
    Given The second number <num1>
    When I divide one number from another
    Then I get the result <result>
    
    Examples:
    | num1 | result |
    | 2    | 50     |
    | 10   | 10     |
    | 25   | 4      |
